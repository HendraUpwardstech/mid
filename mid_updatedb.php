<?php

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "wordpress");
define("DB_TABLE", "mid_raw_data_2013");

ob_start();
$_DatabaseName  = DB_NAME;
$_TableName     = DB_TABLE;
$mid_db         = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
if (!$mid_db)
    die('Could not connect: ' . mysql_error());
mysql_select_db($_DatabaseName);

// Get Government
$government = mysql_query("SELECT * FROM mid_list_government");
$listGovernment = array();
while($data = mysql_fetch_object($government))
{
    $listGovernment[] = $data->name;
}

// Get Nonprofit
$nonprofit = mysql_query("SELECT * FROM mid_list_non_profit");
$listNonprofit = array();
while($data = mysql_fetch_object($nonprofit))
{
    $listNonprofit[] = $data->name;
}

// Get all db
$getAllData = mysql_query("SELECT * FROM $_TableName");

$listExemptByOrdinance = array(
    '7666202292',
    '7666202312',
    '7666202315',
    '7666202317',
    '7666202422',
    '7666202433',
    '7666202487',
    '7666202497',
    '7666202420'
);


$epsilon = 0.00001;

// Fill Ceiling data
while($data = mysql_fetch_object($getAllData))
{
    // Get Calculate Asessment
    $calculateAssessment = null;


    //Base Ceiling
    if($data->TaxpayerName == 'University of Washington')
    {
        $Base_Ceiling = 0;
    }
    elseif($data->TVR == 'OP')
    {
        $Base_Ceiling = (0.32 * $data->LSF) + (0.37 * $data->TAV/1000);
    }
    else
    {
        $Base_Ceiling = (0.32 * $data->LSF) + (0.37 * $data->TTV/1000);
    }

    if($Base_Ceiling > 0)
    {
        $calculateAssessment = $Base_Ceiling;
    }


    //TAV Ceiling
    if($data->TVR == 'OP')
    {
        $TAV_Ceiling = 1.84 * $data->TAV/1000;
    }
    else
    {
        $TAV_Ceiling = 1.84 * $data->TTV/1000;
    }

    if($TAV_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $TAV_Ceiling;
        }
        elseif($TAV_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $TAV_Ceiling;
        }
    }


    //Building Square Footage Ceiling
    if($data->LSF == 0)
    {
        $Building_Square_Footage_Ceiling = 0;
    }
    elseif(($data->BSF/$data->LSF) > 0.5)
    {
        $Building_Square_Footage_Ceiling = 0.17 * $data->BSF;
    }
    else
    {
        $Building_Square_Footage_Ceiling = 0;
    }

    if($Building_Square_Footage_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $Building_Square_Footage_Ceiling;
        }
        elseif($Building_Square_Footage_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $Building_Square_Footage_Ceiling;
        }
    }


    //Hotel Room Ceiling
    $Hotel_Room_Ceiling = $data->HotelCount * 80;

    if($Hotel_Room_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $Hotel_Room_Ceiling;
        }
        elseif($Hotel_Room_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $Hotel_Room_Ceiling;
        }
    }


    //Condo Unit Ceiling
    $Condo_Unit_Ceiling = $data->ResCount * 125;

    if($Condo_Unit_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $Condo_Unit_Ceiling;
        }
        elseif($Condo_Unit_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $Condo_Unit_Ceiling;
        }
    }


    //Apartment Unit Ceiling
    $Apartment_Unit_Ceiling = $data->ResCount * 125;

    if($Apartment_Unit_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $Apartment_Unit_Ceiling;
        }
        elseif($Apartment_Unit_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $Apartment_Unit_Ceiling;
        }
    }


    //Surface Parking TAV Ceiling
    if($data->PUC != 180)
    {
        $Surface_Parking_TAV_Ceiling = 0;
    }
    elseif($data->TVR == 'OP')
    {
        $Surface_Parking_TAV_Ceiling = 0.7 * $data->TAV / 1000;
    }
    else
    {
        $Surface_Parking_TAV_Ceiling = 0.7 * $data->TTV / 1000;
    }

    if($Surface_Parking_TAV_Ceiling > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $Surface_Parking_TAV_Ceiling;
        }
        elseif($Surface_Parking_TAV_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $Surface_Parking_TAV_Ceiling;
        }
    }


    //NP Ceil
    if($data->TVR == 'HP')
    {
        $NPCeil = $Base_Ceiling/4;
    }
    else
    {
        $NPCeil = 0;
    }

    if($NPCeil > 0)
    {
        if($calculateAssessment == null)
        {
            $calculateAssessment = $NPCeil;
        }
        elseif($NPCeil < $calculateAssessment AND $calculateAssessment > 0)
        {
            $calculateAssessment = $NPCeil;
        }
    }


    // Special case
    /*
    if(in_array($data->KCPropId, $listExemptByOrdinance))
    {

    }
    else*/
    if(abs($data->TAV - $data->TTV) > $epsilon)
    {
        if($data->TVR == 'OP')
        {
            $calculateAssessment = $data->TTV;
        }
        elseif($data->TVR == 'HP')
        {
            $calculateAssessment = $data->TAV;
        }
        elseif($data->TVR == 'NP')
        {
            $calculateAssessment = $calculateAssessment * 0.25;
        }
        elseif($data->TVR == 'EX' || $data->TVR == 'MX')
        {
            //Check if owned and operated by goverment
            if(in_array($data->TaxpayerName, $listGovernment))
            {
                $calculateAssessment = 0;
            }
            //Check if owned and operated by nonprofit
            elseif(in_array($data->TaxpayerName, $listNonprofit))
            {
                $calculateAssessment = $calculateAssessment * 0.25;
            }
            else
            {
                $calculateAssessment = $data->TTV;
            }
        }
    }


    // Compare City Assessment with calculate assessment
    $data1 = (float)$data->CityAsessment;
    $data2 = (float)$calculateAssessment;

    $sql = 'UPDATE mid_raw_data_2013 SET NewBaseCeil="'.$Base_Ceiling.'", NewTAVCeil="'.$TAV_Ceiling.'", NewBSFCeil="'.$Building_Square_Footage_Ceiling.'", NewHotelCeil="'.$Hotel_Room_Ceiling.'", NewResCeil="'.$Condo_Unit_Ceiling.'", NewApartCeil="'.$Apartment_Unit_Ceiling.'", NewSPCeil="'.$Surface_Parking_TAV_Ceiling.'", NewNPCeil="'.$NPCeil.'", NewCalculateAssessment="'.$calculateAssessment.'" WHERE DSAID='.$data->DSAID;

    $return = mysql_query($sql) or die(mysql_error());
    var_dump($return);
}

mysql_close($mid_db);
?>