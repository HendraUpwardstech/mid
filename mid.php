<?php

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "wordpress");
define("DB_TABLE", "mid_raw_data_2013");

ob_start();
$_DatabaseName  = DB_NAME;
$_TableName     = DB_TABLE;
$mid_db         = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
if (!$mid_db)
    die('Could not connect: ' . mysql_error());
mysql_select_db($_DatabaseName);

// Get Government
$government = mysql_query("SELECT * FROM mid_list_government");
$listGovernment = array();
while($data = mysql_fetch_object($government))
{
    $listGovernment[] = $data->name;
}

// Get Nonprofit
$nonprofit = mysql_query("SELECT * FROM mid_list_non_profit");
$listNonprofit = array();
while($data = mysql_fetch_object($nonprofit))
{
    $listNonprofit[] = $data->name;
}

// Get all db
$getAllData = mysql_query("SELECT * FROM $_TableName");

$listExemptByOrdinance = array(
    '7666202292',
    '7666202312',
    '7666202315',
    '7666202317',
    '7666202422',
    '7666202433',
    '7666202487',
    '7666202497',
    '7666202420'
);

?>
    <style>
        table,th,td
        {
            border:1px solid black;
        }
        td
        {
            padding: 5px;
        }
    </style>
    <table cellpadding="0" cellspacing="0">
    <tr>
        <th>DSAID</th>
        <th>KCPropId</th>
        <th>ParcelID</th>
        <th>PropName</th>
        <th>Taxpayer Name</th>
        <th>TaxpayerAttnLine</th>
        <th>TaxpayerAddress</th>
        <th>TaxpayerCityStateZip</th>
        <th>PUC</th>
        <th>LUD</th>
        <th>Notes</th>
        <th>Basis</th>
        <th>EstAssess</th>
        <th>PropAdd</th>
        <th>CommercialCondoAsc</th>
        <th>LSF</th>
        <th>BSF</th>
        <th>TAV</th>
        <th>TTV</th>
        <th>ValueForAssessment</th>
        <th>TAVPercent</th>
        <th>PercentOwnership</th>
        <th>TVR</th>
        <th>FAR</th>
        <th>CondoType</th>
        <th>CondoTypeDesc</th>
        <th>Base Ceiling</th>
        <th>TAV Ceiling</th>
        <th>Building Square Footage Ceiling</th>
        <th>Hotel Room Ceiling</th>
        <th>Condo Unit Ceiling</th>
        <th>Apartment Unit Ceiling</th>
        <th>Surface Parking TAV Ceiling</th>
        <th>NPCeil</th>
        <th>CalculateAssessment</th>
        <th>CityAssessment</th>
    </tr>
    <?php

    $epsilon = 0.00001;

    // Fill Ceiling data
    while($data = mysql_fetch_object($getAllData))
    {
        // Get Calculate Asessment
        $calculateAssessment = null;


        //Base Ceiling
        if($data->TaxpayerName == 'University of Washington')
        {
            $Base_Ceiling = 0;
        }
        elseif($data->TVR == 'OP')
        {
            $Base_Ceiling = (0.32 * $data->LSF) + (0.37 * $data->TAV/1000);
        }
        else
        {
            $Base_Ceiling = (0.32 * $data->LSF) + (0.37 * $data->TTV/1000);
        }

        if($Base_Ceiling > 0)
        {
            $calculateAssessment = $Base_Ceiling;
        }


        //TAV Ceiling
        if($data->TVR == 'OP')
        {
            $TAV_Ceiling = 1.84 * $data->TAV/1000;
        }
        else
        {
            $TAV_Ceiling = 1.84 * $data->TTV/1000;
        }

        if($TAV_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $TAV_Ceiling;
            }
            elseif($TAV_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $TAV_Ceiling;
            }
        }


        //Building Square Footage Ceiling
        if($data->LSF == 0)
        {
            $Building_Square_Footage_Ceiling = 0;
        }
        elseif(($data->BSF/$data->LSF) > 0.5)
        {
            $Building_Square_Footage_Ceiling = 0.17 * $data->BSF;
        }
        else
        {
            $Building_Square_Footage_Ceiling = 0;
        }

        if($Building_Square_Footage_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $Building_Square_Footage_Ceiling;
            }
            elseif($Building_Square_Footage_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $Building_Square_Footage_Ceiling;
            }
        }


        //Hotel Room Ceiling
        $Hotel_Room_Ceiling = $data->HotelCount * 80;

        if($Hotel_Room_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $Hotel_Room_Ceiling;
            }
            elseif($Hotel_Room_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $Hotel_Room_Ceiling;
            }
        }


        //Condo Unit Ceiling
        $Condo_Unit_Ceiling = $data->ResCount * 125;

        if($Condo_Unit_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $Condo_Unit_Ceiling;
            }
            elseif($Condo_Unit_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $Condo_Unit_Ceiling;
            }
        }


        //Apartment Unit Ceiling
        $Apartment_Unit_Ceiling = $data->ResCount * 125;

        if($Apartment_Unit_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $Apartment_Unit_Ceiling;
            }
            elseif($Apartment_Unit_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $Apartment_Unit_Ceiling;
            }
        }


        //Surface Parking TAV Ceiling
        if($data->PUC != 180)
        {
            $Surface_Parking_TAV_Ceiling = 0;
        }
        elseif($data->TVR == 'OP')
        {
            $Surface_Parking_TAV_Ceiling = 0.7 * $data->TAV / 1000;
        }
        else
        {
            $Surface_Parking_TAV_Ceiling = 0.7 * $data->TTV / 1000;
        }

        if($Surface_Parking_TAV_Ceiling > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $Surface_Parking_TAV_Ceiling;
            }
            elseif($Surface_Parking_TAV_Ceiling < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $Surface_Parking_TAV_Ceiling;
            }
        }


        //NP Ceil
        if($data->TVR == 'HP')
        {
            $NPCeil = $Base_Ceiling/4;
        }
        else
        {
            $NPCeil = 0;
        }

        if($NPCeil > 0)
        {
            if($calculateAssessment == null)
            {
                $calculateAssessment = $NPCeil;
            }
            elseif($NPCeil < $calculateAssessment AND $calculateAssessment > 0)
            {
                $calculateAssessment = $NPCeil;
            }
        }


        // Special case
        /*
        if(in_array($data->KCPropId, $listExemptByOrdinance))
        {

        }
        else*/
        if(abs($data->TAV - $data->TTV) > $epsilon)
        {
            if($data->TVR == 'OP')
            {
                $calculateAssessment = $data->TTV;
            }
            elseif($data->TVR == 'HP')
            {
                $calculateAssessment = $data->TAV;
            }
            elseif($data->TVR == 'NP')
            {
                $calculateAssessment = $calculateAssessment * 0.25;
            }
            elseif($data->TVR == 'EX' || $data->TVR == 'MX')
            {
                //Check if owned and operated by goverment
                if(in_array($data->TaxpayerName, $listGovernment))
                {
                    $calculateAssessment = 0;
                }
                //Check if owned and operated by nonprofit
                elseif(in_array($data->TaxpayerName, $listNonprofit))
                {
                    $calculateAssessment = $calculateAssessment * 0.25;
                }
                else
                {
                    $calculateAssessment = $data->TTV;
                }
            }
        }


        // Compare City Assessment with calculate assessment
        $data1 = (float)$data->CityAsessment;
        $data2 = (float)$calculateAssessment;

        ?>
        <tr>
            <td><?php echo isset($data->DSAID) ? $data->DSAID : null ?></td>
            <td><?php echo isset($data->KCPropId) ? $data->KCPropId : null ?></td>
            <td><?php echo isset($data->ParcelID) ? $data->ParcelID : null ?></td>
            <td><?php echo isset($data->PropName) ? $data->PropName : null ?></td>
            <td><?php echo isset($data->TaxpayerName) ? $data->TaxpayerName : null ?></td>
            <td><?php echo isset($data->TaxpayerAttnLine) ? $data->TaxpayerAttnLine : null ?></td>
            <td><?php echo isset($data->TaxpayerAddress) ? $data->TaxpayerAddress : null ?></td>
            <td><?php echo isset($data->TaxpayerCityStateZip) ? $data->TaxpayerCityStateZip : null ?></td>
            <td><?php echo isset($data->PUC) ? $data->PUC : null ?></td>
            <td><?php echo isset($data->LUD) ? $data->LUD : null ?></td>
            <td><?php echo isset($data->Notes) ? $data->Notes : null ?></td>
            <td><?php echo isset($data->Basis) ? $data->Basis : null ?></td>
            <td><?php echo isset($data->EstAssess) ? $data->EstAssess : null ?></td>
            <td><?php echo isset($data->PropAdd) ? $data->PropAdd : null ?></td>
            <td><?php echo isset($data->CommercialCondoAsc) ? $data->CommercialCondoAsc : null ?></td>
            <td><?php echo isset($data->LSF) ? $data->LSF : null ?></td>
            <td><?php echo isset($data->BSF) ? $data->BSF : null ?></td>
            <td><?php echo isset($data->TAV) ? $data->TAV : null ?></td>
            <td><?php echo isset($data->TTV) ? $data->TTV : null ?></td>
            <td><?php echo isset($data->ValueForAssessment) ? $data->ValueForAssessment : null ?></td>
            <td><?php echo isset($data->TAVPercent) ? $data->TAVPercent : null ?></td>
            <td><?php echo isset($data->PercentOwnership) ? $data->PercentOwnership : null ?></td>
            <td><?php echo isset($data->TVR) ? $data->TVR : null ?></td>
            <td><?php echo isset($data->FAR) ? $data->FAR : null ?></td>
            <td><?php echo isset($data->CondoType) ? $data->CondoType : null ?></td>
            <td><?php echo isset($data->CondoTypeDesc) ? $data->CondoTypeDesc : null ?></td>
            <td><?php echo ($data->BaseCeil == $Base_Ceiling) ? '<span style="color:green">'.$data->BaseCeil.' = '.$Base_Ceiling.'</span>' : '<span style="color:red">'.$data->BaseCeil.' != '.$Base_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->TAVCeil == $TAV_Ceiling) ? '<span style="color:green">'.$data->TAVCeil.' = '.$TAV_Ceiling.'</span>' : '<span style="color:red">'.$data->TAVCeil.' != '.$TAV_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->BSFCeil == $Building_Square_Footage_Ceiling) ? '<span style="color:green">'.$data->BSFCeil.' = '.$Building_Square_Footage_Ceiling.'</span>' : '<span style="color:red">'.$data->BSFCeil.' != '.$Building_Square_Footage_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->HotelCeil == $Hotel_Room_Ceiling) ? '<span style="color:green">'.$data->HotelCeil.' = '.$Hotel_Room_Ceiling.'</span>' : '<span style="color:red">'.$data->HotelCeil.' != '.$Hotel_Room_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->ResCeil == $Condo_Unit_Ceiling) ? '<span style="color:green">'.$data->ResCeil.' = '.$Condo_Unit_Ceiling.'</span>' : '<span style="color:red">'.$data->CondoCeil.' != '.$Condo_Unit_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->ResCeil == $Apartment_Unit_Ceiling) ? '<span style="color:green">'.$data->ResCeil.' = '.$Apartment_Unit_Ceiling.'</span>' : '<span style="color:red">'.$data->AptCeil.' != '.$Apartment_Unit_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->SPCeil == $Surface_Parking_TAV_Ceiling) ? '<span style="color:green">'.$data->SPCeil.' = '.$Surface_Parking_TAV_Ceiling.'</span>' : '<span style="color:red">'.$data->SPCeil.' != '.$Surface_Parking_TAV_Ceiling.'</span>' ?></td>
            <td><?php echo ($data->NPCeil == $NPCeil) ? '<span style="color:green">'.$data->NPCeil.' = '.$NPCeil.'</span>' : '<span style="color:red">'.$data->NPCeil.' != '.$NPCeil.'</span>' ?></td>
            <td><?php echo (abs($data1-$data2) < $epsilon) ? '<span style="color:green">'.$data->CityAsessment.' = '.$calculateAssessment.'</span>' : '<span style="color:red">'.$data->CityAsessment.' != '.$calculateAssessment.'</span>' ?></td>
            <td><?php echo $data->CityAsessment ?></td>
        </tr>
    <?php

    }

    ?>
    </table>
<?php

mysql_close($mid_db);
?>